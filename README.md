## Ordercloud PHP Example Mobile Stores

These ARE NOT production ready stores, they can be if you put effort in, but for now they are examples. There are 4 different types:

Merchant - 		 This is your single entity store, they sell only their own products.

Franchise - 		 This is usually for a chain stores, where they order from a centralised point, but the order gets processed from different points.
			 It uses 2 entities to fulfill an order, namely a centralized ordering point and the specific store which the order gets assigned to.

Franchise Market Place - This is for a market place with multiple branches in different areas. Has 3 entity points to place an order: 
			 The centralized order point, order gets recieved by the local branch and the 3rd party producing the food.

Market Place - 		 They resell other products without having their own. The 2 entities are the 3rd party producing the product and the order point.

## Why am I talking about entities?

The Ordercloud environment is built as a centralized integration point, all above mentioned use cases are supported, but they work a bit differently in flow. Entities 
are the organisations, so where I say 3 entities, it means 3 organisations should be connected to setup the environment.

## Tech stuff

Firstly, NO SQL!!! As the products, orders and users live in the API, we don't have a DB, there is Redis for the cart data.

The frontend is [Jquery Mobile 1.3.1](http://api.jquerymobile.com/1.3/) and in there are a few themes in graphite-themes, just replace the CSS.

Laravel 4.2 is used, Laravel's README is in each project.

Email willem@ordercloud.co.za for any further queries.