<html>
    <head>
        <title>King Delivery Mobile Store</title>
        <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="apple-touch-icon" sizes="57x57" href="/apple-touch-icon-57x57.png">
		<link rel="apple-touch-icon" sizes="114x114" href="/apple-touch-icon-114x114.png">
		<link rel="apple-touch-icon" sizes="72x72" href="/apple-touch-icon-72x72.png">
		<link rel="apple-touch-icon" sizes="144x144" href="/apple-touch-icon-144x144.png">
		<link rel="apple-touch-icon" sizes="60x60" href="/apple-touch-icon-60x60.png">
		<link rel="apple-touch-icon" sizes="120x120" href="/apple-touch-icon-120x120.png">
		<link rel="apple-touch-icon" sizes="76x76" href="/apple-touch-icon-76x76.png">
		<link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon-152x152.png">
		<link rel="icon" type="image/png" href="/favicon-196x196.png" sizes="196x196">
		<link rel="icon" type="image/png" href="/favicon-160x160.png" sizes="160x160">
		<link rel="icon" type="image/png" href="/favicon-96x96.png" sizes="96x96">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="msapplication-TileImage" content="/mstile-144x144.png">

        <link rel="stylesheet" href="/css/jquery.mobile-1.3.1.css" />
        <link rel="stylesheet" href="/css/custom.css" />
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="/js/custom.js"></script>
        <script src="http://code.jquery.com/mobile/1.3.1/jquery.mobile-1.3.1.min.js"></script>
        <!--<link rel="stylesheet" href="/css/jquery.mobile.icons.min.css" />
        <link rel="stylesheet" href="/css/jquery.mobile-1.4.3.css" />
        <link rel="stylesheet" href="/css/kd.min.css" />
        <link rel="stylesheet" href="/css/custom.css" />
		<script type="text/javascript" src="/js/jquery-1.9.1.min.js"></script>
		<script type="text/javascript" src="/js/jquery.validate.js" ></script>

		<script type="text/javascript" src="/js/jquery.mobile-1.4.0.min.js"></script>
		<script type="text/javascript" src="/js/jquery.mobile.message.js"></script>-->
    </head>
    <body>
        <div data-role="page">
            <div data-theme="a" data-role="panel" id="mypanel" data-display="overlay">
                <ul data-theme="c" data-role="listview" data-inset="true" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="@if(isset($selectedStoreDetails) && $selectedStoreDetails != false) /home?store={{$selectedStoreDetails['id']}} @else /home @endif" class="ui-link-inherit">Menu</a>
                    </li>
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="/" class="ui-link-inherit">Shops</a>
                    </li>
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="/cart" class="ui-link-inherit">Cart @if(isset($count[0]->items)) ({{$count[0]->items}})@endif </a>
                    </li>
                    @if(isset($user) && $user !== null && $user !== false)
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="/myorders" class="ui-link-inherit">Orders</a>
                    </li>
                    @endif
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="/home" class="ui-link-inherit">Branches</a>
                    </li>
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        <a href="/ABOUT" class="ui-link-inherit">About</a>
                    </li>
                    <li data-corners="false" data-shadow="false" data-iconshadow="true" data-icon="arrow-r" data-iconpos="right" data-theme="c" class="ui-btn ui-btn-icon-right ui-li-has-arrow ui-li ui-btn-up-a">
                        @if(!isset($user) || ($user === null || $user === false))
			                <a data-role="none" rel="external" href="/register?login=1" class="ui-link-inherit">Login</a>
                        @else
			                <a href="/logout" data-role="none" rel="external" class="ui-link-inherit">Logout</a>
                        @endif
                    </li>
                </ul>
            </div>
            <div data-role="header" data-theme="a">
	            <div style="background: none repeat scroll 0 0 #000;" class="ui-header ui-bar-a" data-swatch="a" data-theme="a" data-form="ui-bar-a" data-role="header" role="banner">
                    <img style="padding: 20px;" src="/img/number.png">
                    @if(!isset($user) || $user === null || $user === false || !isset($user["profile"]["firstName"]))
                        <p style="float: right; padding-right: 20px; text-align: right; padding-top: 8px;">
                            <a style="color: rgb(255, 255, 255);cursor: auto;display: inline;font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 18px;font-weight: bold;height: auto;line-height: 13px;list-style-image: none;list-style-position: outside;list-style-type: none;text-align: left;text-decoration: none solid rgb(132, 132, 132);text-transform: uppercase;width: auto;" data-role="none" rel="external" href="/register?login=1" class="ui-link">Login</a><br><br>
                            <a style="color: rgb(255, 255, 255);cursor: auto;display: inline;font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 18px;font-weight: bold;height: auto;line-height: 13px;list-style-image: none;list-style-position: outside;list-style-type: none;text-align: left;text-decoration: none solid rgb(132, 132, 132);text-transform: uppercase;width: auto;" data-role="none" rel="external" href="/register?login=0" class="ui-link">register</a>
                        </p>
                    @else
                    <p style="float: right; ">
                        <a style="color: rgb(132, 132, 132);cursor: auto;display: inline;font-family: 'Trebuchet MS', Arial, Helvetica, sans-serif;font-size: 13px;font-weight: bold;height: auto;line-height: 13px;list-style-image: none;list-style-position: outside;list-style-type: none;text-align: left;text-decoration: none solid rgb(132, 132, 132);text-transform: uppercase;width: auto;" data-role="none" rel="external" href="/register?login=1" class="ui-link">Hi: {{$user["profile"]["firstName"]}}</a>
                    </p>
                    @endif
                    @if(isset($storeStatus))
                        <div style="float: left; margin-left: 15px; margin-bottom: 4px; margin-top: -10px;">
                            <p style="display: inline; padding-left: 5px;">{{$selectedStoreDetails['name']}}: {{$storeStatus["status"]}}</p>
                            <img width="20px;" style="vertical-align: middle; margin-left: 7px; margin-bottom: 2px;" src="/img/{{$storeStatus["colour"]}}.png">
                        </div>
                        <br>
                        <br>
                    @endif
                </div>
            </div>
            <div data-role="header" data-theme="a">
                <div class="ui-header ui-bar-a" data-swatch="a" data-theme="a" data-form="ui-bar-a" data-role="header" role="banner">
                    <a href="#mypanel" data-role="button" data-icon="delete" data-iconpos="notext" data-mini="true" data-inline="true" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="a" title="Delete" class="ui-btn ui-shadow ui-btn-corner-all ui-mini ui-btn-inline ui-btn-icon-notext ui-btn-up-a">
                        <span class="ui-btn-inner">
                            <span class="ui-icon ui-icon-grid ui-icon-shadow">&nbsp;</span>
                        </span>
                    </a>
                   @if(isset($user) && $user !== null && $user !== false && isset($count) && $count !== 0)
                        <a id="cart" href="/cart" data-role="button" class="ui-btn-right ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a" data-corners="true" data-shadow="true" data-iconshadow="true" data-wrapperels="span" data-theme="a"><span class="ui-btn-inner"><span class="ui-btn-text">Cart ({{$count}})</span></span></a>
                    @endif
                    <h1 class="ui-title" tabindex="0" role="heading" aria-level="1"></h1>
                </div>
            </div>
            @yield('content')
            <div data-role="footer" id="pageFooter">
                <h4>2014 &copy; King Delivery</h4>
            </div>
        </div>
    </body>
</html>
