@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
    <div data-theme="a" data-form="ui-body-a" class="ui-body ui-body-a ui-corner-all">
        <h2>Select a Payment Method</h2>
        <fieldset class="ui-corner-all ui-body-c ui-controlgroup ui-controlgroup-vertical" data-role="controlgroup">
            <input checked="checked" type="radio" name="radio-choice-1" id="radio-choice-2" value="choice-2">
            <label for="radio-choice-2">Cash on Delivery</label>
            <input disabled="disabled" type="radio" name="radio-choice-1" id="radio-choice-1" value="choice-1">
            <label for="radio-choice-1">Credit Cart (coming soon!)</label>
            <input disabled="disabled" type="radio" name="radio-choice-1" id="radio-choice-3" value="choice-3">
            <label for="radio-choice-3">InstantEFT (coming soon!)</label>
            <input disabled="disabled" type="radio" name="radio-choice-1" id="radio-choice-4" value="choice-4">
            <label for="radio-choice-4">Account (coming soon!)</label>
        </fieldset>
        <a href="/order/{{$geoId}}" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
            <span class="ui-btn-inner">
                <span class="ui-btn-text">Order</span>
            </span>
        </a>
    </div>
</div>
@stop