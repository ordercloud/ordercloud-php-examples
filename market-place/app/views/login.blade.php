@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	<form action="/login" method="POST" id="login">
		<label for="email">Email:</label>
		<input type="text" name="email" id="email" value="">
		<label for="password">Password:</label>
		<input type="password" name="password" id="password" value="" autocomplete="off">
		<button type="submit" class="ui-btn ui-body-a">Login</button>
	</form>
</div>
@stop