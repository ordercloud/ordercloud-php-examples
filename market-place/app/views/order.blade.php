@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	<div class="ui-bar ui-bar-c ui-corner-all" style="margin-bottom:1em;">
		<h2>Your order has been placed!</h2><br>
		<a href="/myorders">Track my Orders</a>
	</div>
</div>
@stop