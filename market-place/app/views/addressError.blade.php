@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	We were unable to locate your location, error: {{$error}}
	<br><br>
	Click <a href="/address">here</a> to add your address or <a href="/geo">here</a>
</div>
@stop