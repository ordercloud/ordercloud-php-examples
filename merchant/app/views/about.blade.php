@extends('layouts.master')
@section('content')
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.1/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.1/mapbox.css' rel='stylesheet' />
<script type="text/javascript">
	$( document ).on("pageshow", function () {

		L.mapbox.accessToken = 'pk.eyJ1Ijoid2lsbGVtIiwiYSI6IjRBa2J4NEUifQ.xPT5ryIUBy1vWTs0vHOIOA';

		var map = L.map('map').setView([{{$store["profile"][0]["latitude"]}}, {{$store["profile"][0]["longitude"]}}], 17);

		L.tileLayer('https://{s}.tiles.mapbox.com/v3/examples.map-i87786ca/{z}/{x}/{y}.png', {
			maxZoom: 18,
			id: 'examples.map-i86knfo3'
		}).addTo(map);

		var mark = L.marker([{{$store["profile"][0]["latitude"]}}, {{$store["profile"][0]["longitude"]}}], {draggable: false, clickable: false}).addTo(map);
    });
</script>
<div data-role="content" class="ui-content ui-body-c" id="slider-wrapper" role="main">
	<div data-theme="a" data-form="ui-body-a" class="ui-body ui-body-a ui-corner-all">
		<h2 style="font-size: 1.3em;">{{$store["name"]}}</h2>
        @if(!empty($store["operatingHours"]))
            <ul data-role="listview" data-inset="true" data-filter="false" data-input="#filter-for-listview">
                @foreach ($store["operatingHours"] as $hour)
                <li>{{$days[$hour["day"]]}}: {{$hour["openTime"]}} - {{$hour["closeTime"]}}</li>
                @endforeach
            </ul>
        @endif
        @if(!empty($store["profile"]))
        <p>
            Contact: {{$store["profile"][0]["contactPerson"]["profile"]["firstName"]}}<br>
            Telephone: {{$store["profile"][0]["contactPerson"]["profile"]["cellphoneNumber"]}}
        </p>
        @endif
        <div  style="min-height: 300px; max-width: 800px; margin: 0 auto;" id="map"></div>
    </div>
</div>
@stop