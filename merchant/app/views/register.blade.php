@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	<form action="/register" method="POST" value="" class="form" id="register">
		<fieldset data-role="fieldcontain"> 
			<label for="email">Email:</label>
			<input type="text" name="email" id="email" value="{{$userData['email']}}" class="required email">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="name">Name:</label>
			<input type="text" name="first_name" id="name" value="{{$userData['first_name']}}" class="required">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="surname">Surname:</label>
			<input type="text" name="surname" id="surname" value="{{$userData['surname']}}" class="required">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="mobile">Mobile:</label>
			<input type="text" name="mobile" id="mobile" value="{{$userData['mobile']}}" class="required">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="password">Password:</label>
			<input type="password" name="password" id="password" value="" autocomplete="off" class="required">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="password_comfirm">Confirm Password:</label>
			<input type="password" name="password_comfirm" id="password_comfirm" value="" autocomplete="off"  class="required">
		</fieldset>
		<fieldset data-role="fieldcontain"> 
			<label for="email">Sex:</label>
			<select name="sex" id="sex" data-role="slider" data-mini="true">
			    <option value="M" @if($userData['sex'] == "male") selected="selected" @endif>M</option>
			    <option value="F" @if($userData['sex'] == "female") selected="selected" @endif>F</option>
			</select>
		</fieldset>
		<button type="submit" class="ui-btn ui-body-a ajaxPost">Register</button>
	</form>
	<a data-ajax="false" href="/social/facebook" class="ui-btn ui-body-a">Login with Facebook</a>
</div>
@stop