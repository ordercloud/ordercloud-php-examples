<?php
use Ordercloud\Ordercloud\Ordercloud;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Auth;

class HomeController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    protected $layout = "layouts.master";

    public function logout()
    {
        Cookie::queue('user', null, 1);
        Cookie::queue('access_token', null, 1);
        Cookie::queue('refresh_token', null, 1);
        return Redirect::to("/");
    }

    public function categories()
    {
        $selectedStoreDetails = false;
        if(Cookie::has("selectStore"))
        {
            $selectedStoreDetails = Cookie::get("selectStore");

        }
        return View::make('category', array("store" => $selectedStoreDetails));
    }

    public function landing()
    {

        $selectedStoreDetails = false;
        if(Cookie::has("selectStore"))
        {
            $selectedStoreDetails = Cookie::get("selectStore");

        }
        return View::make('category', array("store" => $selectedStoreDetails));
    }

    public function products($category)
    {
        $accessToken = Cookie::get("access_token");
        $oc = new Ordercloud();

        if(Cookie::get("user") != false && $accessToken != false)
        {
            $products = $oc->getProductsByMarketPlace(Cookie::get("selectStore")["id"], $category, Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $products = $oc->getProductsByMarketPlace(Cookie::get("selectStore")["id"], $category, Ordercloud::AUTH_TYPE_BASIC);
        }
        $productBuild = array();

        foreach($products AS $product)
        {
            $productBuild[$product['id']] = $product;
        }
        return View::make('productList', array("category" => (int)$category, "products" => $productBuild));
    }
    
    public function addCart($item_id)
    {
        $oc = new Ordercloud();
        $accessToken = Cookie::get("access_token");
        if(Cookie::get("user") != false && $accessToken != false)
        {
            $products = $oc->getProduct($item_id, Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $products = $oc->getProduct($item_id, Ordercloud::AUTH_TYPE_BASIC);
        }

        $redis = Redis::connection();
        $orgs = $redis->get("product_orgs_" . unserialize(Cookie::get("user"))["id"]);
        if($orgs !== false)
        {
            $orgs = unserialize($orgs);
            $orgs[] = $products["organisation"]["id"];
        }
        else
        {
            $orgs = array($products["organisation"]["id"]);
        }
        $redis->set("product_orgs_" . unserialize(Cookie::get("user"))["id"], serialize($orgs));

        $user = $redis->get("user_" . unserialize(Cookie::get("user"))["id"]);
        $cartItem = array(
            "created" => date("Y-m-d H:i:s"),
            "item_id" =>  $products['id'],
            "sku" => "SKU123",
            "price" => $products['price'],
            "name" => $products['name'],
            "org" => $products["organisation"]["id"]
        );
    	$cookieUser = unserialize(Cookie::get("user"));
    	if(empty(json_decode($user, true)["cart_items"]))
		{
            //To who ever has to look at this ever again... I am just setting the values in this ugly piece of one line code
            $redis->set("user_" . $cookieUser["id"], json_encode(array(
                "cart" => array("created" => date("Y-m-d H:i:s"), "cart_total" => $cartItem["price"]),
                "cart_items" => array($cartItem)
            )));
    	}
    	else 
    	{
            $existingData = json_decode($user, true);;
            $cart = $existingData["cart"];
            $cart["cart_total"] += $cartItem["price"];
            $items = $existingData["cart_items"];
            $items[] = $cartItem;
            $newData = array();
            $newData["cart"] = $cart;
            $newData["cart_items"] = $items;


            $redis->set("user_" . $cookieUser["id"], json_encode($newData));
    	}
    	return Response::json(array('success' => true, "message" => "Added to Cart!"));
    }
    
    public function cartGet()
    {
        $cartData = json_decode(Redis::connection()->get("user_" . unserialize(Cookie::get("user"))["id"]), true)['cart_items'];

        $orgs = unserialize(Redis::get("product_orgs_" . unserialize(Cookie::get("user"))["id"]));
        $orgIds = array_unique($orgs);
        $oc = new Ordercloud();
        $message = array();
        $orderable = true;
        $deliverable = true;

        //So now we need to check the shop statuses
        foreach($orgIds AS $orgId)
        {
            $org = $oc->getStore($orgId);
            //var_dump($org);
            if($org["open"] === false)
            {
                $orderable = false;
                $message[] = $org['name'] . " is currently closed, please remove items from this store before ordering";
            }
            else if($org["delivering"] === false)
            {
                $deliverable = false;
                $message[] = $org['name'] . " is currently unable to delivery, but still open for self pick up";
            }
        }

        $totalCount = 0;
        $itemCount = array();
        $sortedCartData = array();
        $cartTotal = 0;
        if(!is_array($cartData))
        {
            return Response::view('404', array(), 404);
        }
        foreach($cartData as $item)
        {
            $totalCount++;
            $sortedCartData[$item["item_id"]] = $item;
            $cartTotal += $item["price"];
            if(isset($itemCount[$item["item_id"]]))
            {
                $itemCount[$item["item_id"]]++;
            }
            else
            {
                $itemCount[$item["item_id"]] = 1;
            }
        }

    	return View::make('cart', array(
    			"cartItems" => $sortedCartData,
                "totalCount" => $totalCount,
                "itemCount" => $itemCount,
                "cartTotal" => $cartTotal,
                "message" => $message,
                "orderable" => $orderable,
                "deliverable" => $deliverable
            )
		);
    }
    
    public function cartPost()
    {
    	$data = Input::all();
        $redis = Redis::connection();

        $cartItems = json_decode($redis->get("user_" . unserialize(Cookie::get("user"))["id"]), true);

        $currentItems = array();
        foreach($cartItems["cart_items"] AS $cartItem)
        {
            $currentItems[$cartItem["item_id"]] = isset($currentItems[$cartItem["item_id"]]) ? $currentItems[$cartItem["item_id"]] + 1 : 1;
        }

        if($data["product"] != $currentItems)
        {
            $sorted = array();

            foreach($cartItems["cart_items"] AS $key => $item)
            {
                $sorted[$item["item_id"]][] = $key;
            }

            foreach($data['product'] as $id => $count)
            {
                $itemCountDb = $currentItems[$id];
                if($itemCountDb > $count)
                {
                    //If there are more items is the db than being submitted back, we must remove
                    $toRemove = $itemCountDb - $count;
                    for($i = 0; $i < $toRemove; $i++)
                    {
                        reset($sorted[$id]);
                        $first_key = key($sorted[$id]);
                        unset($cartItems["cart_items"][$sorted[$id][$first_key]]);
                        unset($sorted[$id][$first_key]);
                    }

                }
                else if($itemCountDb < $count)
                {
                    //If there are less items is the db than being submitted back, we must add
                    $toAdd = $count - $itemCountDb;
                    reset($sorted[$id]);
                    $first_key = key($sorted[$id]);
                    $singleItem = $cartItems["cart_items"][$sorted[$id][$first_key]];
                    unset($cartItems["cart_items"][$sorted[$id][$first_key]]);
                    for($i = 0; $i <= $toAdd; $i++)
                    {
                        $cartItems["cart_items"][] = $singleItem;
                    }
                }
            }
            $redis->set("user_" . unserialize(Cookie::get("user"))["id"], json_encode($cartItems));
            return MY_response::redirect("/geo");
        }
        else
        {

            return MY_response::redirect("/geo");
        }

        return Response::view('404', array(), 404);
    }
    
    public function removeFromCart($itemId)
    {
        $deleted = 0;
        $redis = Redis::connection();
        //We just remove the
        $orgs = $redis->get("product_orgs_" . unserialize(Cookie::get("user"))["id"]);
        $cartItems = json_decode($redis->get("user_" . unserialize(Cookie::get("user"))["id"]), true);
        $finalitems = $cartItems;
        if ($orgs !== false) {
            $orgs = unserialize($orgs);
            foreach ($cartItems["cart_items"] AS $key => $item) {
                if ($item["item_id"] == $itemId) {
                    $key = array_search($item["org"], $orgs);
                    if ($key !== false) {
                        unset($orgs[$key]);
                    }
                }
                unset($finalitems["cart_items"][$key]);
                $deleted = 1;
            }
        }

        $redis->set("product_orgs_" . unserialize(Cookie::get("user"))["id"], serialize($orgs));
        $redis->set("user_" . unserialize(Cookie::get("user"))["id"], json_encode($finalitems));
    	return Response::json(array('success' => $deleted > 0 ? true : false, "message" => "Removed from Cart!"));
    }
    
    public function product($productId)
    {
        $redis = Redis::connection();
        $oc = new Ordercloud();
        $accessToken = Cookie::get("access_token");

        if(Cookie::get("user") != false && $accessToken != false)
        {
            $products = $oc->getProduct($productId, Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $products = $oc->getProduct($productId, Ordercloud::AUTH_TYPE_BASIC);
        }

        return View::make('product',
            array(
                "product" => $products
            )
        );

    }

    public function cart($productId)
    {
    	return View::make('cart');
    }
    
    public function registerGet()
    {
        $login = Input::get("login") == "1" ? true : false;
        $cookie = Cookie::make("redirect_url", Input::get("redirect_url") != false ? Input::get("redirect_url") : "/", 60);

        $oc = new Ordercloud();
        $url = $oc->getOAuthUrl(action('HomeController@loginGet'), $login, true, Config::get("Ordercloud.client_secret"), Config::get("Ordercloud.organisation_id"));
    	
    	return Redirect::to($url)->withCookie($cookie);
    }

    
    public function loginGet()
    {
        $accessToken = isset(Input::all()["access_token"]) ? Input::all()["access_token"] : "";
    	$refreshToken = isset(Input::all()["refresh_token"]) ? Input::all()["refresh_token"] : "";

    	if(empty(Input::all()["access_token"]) || empty(Input::all()["refresh_token"]))
    	{
            return Response::view('404', array(), 404);
    	}

        $oc = new Ordercloud();
        $response = $oc->getUserDetails($accessToken);

        //var_dump($response); die();

        $user = new User();
        $user["id"] = $response['id'];
        $user->email = $response['profile']["email"];
        $user->first_name = $response['profile']["firstName"];
        $user->last_name = $response['profile']["surname"];
        $user->mobile = $response['profile']["cellphoneNumber"];
    	
    	Auth::login($user);
    	
    	Cookie::queue("access_token", $accessToken, 60);
    	Cookie::queue("user", serialize($response), 60);
    	Cookie::queue("refresh_token", $refreshToken);
    	//Redirect to home page
        if(Cookie::get("redirect_url") === null)
        {
    	    return Redirect::to("/")->with('flash_error', 'Logged in!');
        }
        else
        {
            $url = Cookie::get("redirect_url");
            Cookie::queue('redirect_url', null, 1);
            return Redirect::to($url)->with('flash_error', 'Logged in!');
        }
    }
    
    public function geo ()
    {
    	return View::make('geo');
    }
    
    public function addressGet ()
    {
        $accessToken = Cookie::get("access_token");
        $userId = unserialize(Cookie::get("user"))["id"];

        $oc = new Ordercloud();

        if(Cookie::get("user") !== null && $accessToken !== null)
        {
            $response = $oc->getUserAddresses($userId, Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $response = $oc->getUserAddresses($userId, Ordercloud::AUTH_TYPE_BASIC);
        }
        //get the geos for user
    	$input = Input::all();

    	$geodata = array(
			"house_number" => "",
	   		"street" => "",
	   		"town_city" => "",
	   		"postcode"  => "",
    		"district_region" => ""	
    	);

    	if(isset($input["latlng"]))
    	{
	    	$geo = 'http://maps.google.com/maps/api/geocode/xml?latlng=' . $input["latlng"] . '&sensor=true';
	    	$xml = simplexml_load_file($geo);
	    	 
	    	foreach($xml->result->address_component as $component)
	    	{
	    		if($component->type=='street_number'){
	    			$geodata['house_number'] = $component->long_name;
	    		}
	    		if($component->type=='route'){
	    			$geodata['street'] = $component->long_name;
	    		}
	    		if($component->type=='locality'){
	    			$geodata['town_city'] = $component->long_name;
	    		}
	    		if($component->type=='postal_code'){
	    			$geodata['postcode'] = $component->long_name;
	    		}
	    		if($component->type=='administrative_area_level_3'){
	    			$geodata['district_region'] = $component->long_name;
	    		}
	    	}
            $latlong = explode(",", $input["latlng"]);
    	}
        else
        {
            $latlong = array();
        }
    	return View::make('address', array("geodata" => $geodata, "latlong" => $latlong, "addresses" => $response));
    }

    public function addressPost()
    {
        //Here we need to save the address details
        $data = array();

        $inputData = Input::all();
        $data["longitude"] =  $inputData["long"];
        $data["latitude"] =  $inputData["lat"];
        $data["streetNumber"] = $inputData['street_number'];
        $data["complex"] = $inputData['complex'];
        $data["postalCode"] = $inputData['post_code'];

        $userId = unserialize(Cookie::get("user"))["id"];

        $oc = new Ordercloud();
        $accessToken = Cookie::get("access_token");
        if(Cookie::has("user"))
        {
            $id = $oc->createAddressForUser($userId, $inputData['street_number'] . ", " . $inputData['street_name'], $inputData['street_name'], $inputData['city'], $data, Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $id = $oc->createAddressForUser($userId, $inputData['street_number'] . ", " . $inputData['street_name'], $inputData['street_name'], $inputData['city'], $data, Ordercloud::AUTH_TYPE_BASIC);
        }
        return Redirect::to("/payments/$id");
    }


    public function loginPost()
    {
    	//Validate
    	return View::make('login');
    }
    
    public function social($action)
    {
    	// check URL segment
		if ($action == "auth") {
			// process authentication
			try {
				Hybrid_Endpoint::process();
			}
			catch (Exception $e) {
				// redirect back to http://URL/social/
				return Redirect::route('hybridauth');
			}
			return;
		}
		
		try {
			// create a HybridAuth object
			$socialAuth = new Hybrid_Auth(app_path() . '/config/hybridauth.php');
			// authenticate with Google
			$provider = $socialAuth->authenticate("facebook");
			// fetch user profile
			$userProfile = $provider->getUserProfile();
		}
		catch(Exception $e) {
			// exception codes can be found on HybBridAuth's web site
			return $e->getMessage();
		}
		
		$userData = array(
				"email" => $userProfile->email,
				"phone" => $userProfile->phone,
				"mobile" => $userProfile->phone,
				"facebook_id" => $userProfile->identifier,
				"first_name" => $userProfile->firstName,
				"surname" => $userProfile->lastName,
				"sex" => $userProfile->gender
				
		);
		
		return Redirect::to('/register?userData=' . urlencode(serialize($userData)));
    }
    
    public function addressError()
    {
    	return View::make('addressError', array("error" => Input::get('e')));
    }

    public function checkout()
    {
    	$cartModel = new Cart();
    	return View::make('checkout', 
    		array(
    			"cartItems" => $cartModel->getAllCartItemsForUser(unserialize(Cookie::get("user"))["id"])
    		)
    	);
    }
    
    public function order ($geoId)
    {
        $redis = Redis::connection();
        $data = json_decode($redis->get("user_" . unserialize(Cookie::get("user"))["id"]), true);

        $items = array();
        $total = 0;

        foreach($data["cart_items"] as $item)
        {
            $total += $item["price"];
            if(isset($items[$item["item_id"]]))
            {
                ++$items[$item["item_id"]]["quantity"];
                $items[$item["item_id"]]["price"] = $items[$item["item_id"]]["quantity"] * $item["price"];
            }
            else
            {
                $items[$item["item_id"]]["id"] = $item["item_id"];
                $items[$item["item_id"]]["quantity"] = 1;
                $items[$item["item_id"]]["price"] = $item["price"];
            }
        }

        $jsonSucksBallsArray = array();
        foreach($items AS $item)
        {
            $jsonSucksBallsArray[] = $item;
        }

        $oc = new Ordercloud();
        if($geoId == "none")
        {
            $oc->createOrder(unserialize(Cookie::get("user"))["id"], $jsonSucksBallsArray, Ordercloud::PAYMENT_STATUS_UNPAID, Ordercloud::DELIVERY_TYPE_SELFPICKUP, $total);
        }
        else
        {
            $oc->createOrder(unserialize(Cookie::get("user"))["id"], $jsonSucksBallsArray, Ordercloud::PAYMENT_STATUS_UNPAID, Ordercloud::DELIVERY_TYPE_DELIVERY, $total, $geoId);
        }

        $redis->set("user_" . unserialize(Cookie::get("user"))["id"], json_encode(array("cart" => array(), "cart_items" => array())));
        $redis->set("product_orgs_" . unserialize(Cookie::get("user"))["id"], null);
        return View::make('order',
            array(

            )
        );
    }
    
    public function myorders ()
    {
        $oc = new Ordercloud();
        $accessToken = Cookie::get("access_token");
        if(Cookie::get("user") !== null && $accessToken !== null)
        {
            $response = $oc->getOrderForUser(unserialize(Cookie::get("user"))["id"], Ordercloud::AUTH_TYPE_TOKEN, $accessToken);
        }
        else
        {
            $response = $oc->getOrderForUser(unserialize(Cookie::get("user"))["id"], Ordercloud::AUTH_TYPE_BASIC);
        }


        return View::make('myorders',
            array(
                "orders" => $response
            )
        );
    }

    public function payments($geoId = "none")
    {

        return View::make('payments', array("geoId"=>$geoId));
    }

    public function store ()
    {
        return View::make('store');
    }

    public function about ()
    {
        $days = array(
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday",
            "Sunday",
            "Public Holidays"
        );
        $oc = new Ordercloud();
        //var_dump(Cookie::get("selectStore")["id"]); die();
        $store = $oc->getStore(Cookie::get("selectStore")["id"]);
        return View::make('about', array("store" => $store, "days" => $days));
    }
}