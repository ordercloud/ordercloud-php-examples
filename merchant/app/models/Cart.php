<?php

class Cart extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cart';

	public function scopeCartItems($query, $userId)
	{
		return DB::select("
			SELECT COUNT(cart_item.id) AS items 
			FROM cart
			INNER JOIN cart_item 
			ON cart.id = cart_item.cart_id
			WHERE cart.user_id = " . $userId
		);
	}
	
	public function getAllCartItemsForUser($userId)
	{
		return DB::select("
			SELECT ci.id, ci.cart_id, ci.item_id, ci.sku AS items, ci.price, ci.name, COUNT(ci.item_id) AS item_count, COUNT(ci.item_id) * ci.price AS item_total
			FROM cart_item AS ci
			INNER JOIN cart AS c
			ON c.id = ci.cart_id
			WHERE c.user_id = $userId
			GROUP BY ci.item_id"
		);
	}

    public function getItemsForOrder($userId)
    {
        return DB::select("
			SELECT ci.item_id AS id, COUNT(ci.item_id) AS quantity, COUNT(ci.item_id) * ci.price AS price
            FROM cart_item AS ci
            INNER JOIN cart AS c
            ON c.id = ci.cart_id
            WHERE c.user_id = $userId
            GROUP BY ci.item_id"
        );
    }

    public function clearUserCart($userId)
    {
        return DB::delete("DELETE FROM cart WHERE user_id = ?", array($userId));
    }
}