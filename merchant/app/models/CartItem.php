<?php

class CartItem extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'cart_item';
	
	public function __construct()
	{
		$this->timestamps = false;
	}
}