<?php
use Ordercloud\Ordercloud\Ordercloud;

/*
|--------------------------------------------------------------------------
| Application & Route Filters
|--------------------------------------------------------------------------
|
| Below you will find the "before" and "after" events for the application
| which may be used to do any work before or after a request into your
| application. Here you may also register your custom route filters.
|
*/

App::before(function($request)
{
	//
});


App::after(function($request, $response)
{
	//
});

/*
|--------------------------------------------------------------------------
| Authentication Filters
|--------------------------------------------------------------------------
|
| The following filters are used to verify that the user of the current
| session is logged into this application. The "basic" filter easily
| integrates HTTP Basic authentication for quick, simple checking.
|
*/

Route::filter('auth', function()
{
	if (Cookie::get("user") === false || Cookie::get("user") === null) {
		if(Request::ajax())
		{
			return MY_response::redirect('/register');
		}
		else{
			return Redirect::to("/register");
		}
    }

});

/**
 * So we only have one store, so we refresh it every 30 min
 */
Route::filter('storeSelect', function()
{

	if (Cookie::get("selectStore") === false || Cookie::get("selectStore") === null) {
		$oc = new Ordercloud();
		$selectedStoreDetails = $oc->getStore(Config::get("Ordercloud.organisation_id"));
		Cookie::queue("selectStore", $selectedStoreDetails, 1800);
	}

});


Route::filter('auth.basic', function()
{
	return Cookie::get("user") == false;
});

/*
|--------------------------------------------------------------------------
| Guest Filter
|--------------------------------------------------------------------------
|
| The "guest" filter is the counterpart of the authentication filters as
| it simply checks that the current user is not logged in. A redirect
| response will be issued if they are, which you may freely change.
|
*/

Route::filter('guest', function()
{
	if (Cookie::get("user") == false) return MY_response::redirect('/register');
});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function()
{
	if (Session::token() != Input::get('_token'))
	{
		throw new Illuminate\Session\TokenMismatchException;
	}
});