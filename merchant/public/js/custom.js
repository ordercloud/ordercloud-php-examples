$( document ).on("pageshow", function () {

    $( "#sideButton" ).click(function(e){
        $( "#mypanel" ).trigger( "updatelayout" );
    });

    $("[data-role='collapsible']").collapsible({
	    collapse: function( event, ui ) {
	        $(this).children().next().slideUp(500);
	    },
	    expand: function( event, ui ) {
	        $(this).children().next().hide();
	        $(this).children().next().slideDown(500);
	    }
	});

	$(".ajaxGet").click(function(e){

        e.preventDefault();
		var $link = $(this).attr('href');
		ajaxGet($link);
        if($link.indexOf("add_cart") > -1)
        {
            amountString = $("#cart span span").html().replace( /\D/gi, '');
            amount = parseInt(amountString);
            amount++;
            $("#cart span span").html("Cart (" + amount + ")");
        }
		$('html, body').animate({scrollTop : 0},800);
	});

	$(".ajaxPost").click(function(e){
        var $link = $(this).data('link');
        $("#message-container").html("Loading...");
        ajaxPost($link);
        $('html, body').animate({scrollTop : 0},800);
        e.preventDefault();
	});

    $(".removeFromCart").click(function(e){
    	var $link = $(this).attr('href');
		var success = ajaxGet($link);
        e.preventDefault();
		location.reload();
    });
} );

function showMessage(data)
{
	$( "#message-container" ).slideDown( "slow", function() {

        $("#message-container h2").html(data.message);
        $("#message-container").attr("style", "");
	});
}

function ajaxGet($link)
{
	var success = false
	success = $.ajax({
	  url: $link,
	  type: "GET"
	}).done(function(data, textstatus, xhrReq) {
        if(xhrReq.status == 278)
        {
            window.location.replace(xhrReq.getResponseHeader('Location'));
        }
        else
        {
            showMessage(data);
            if(data.success == true)
            {
                return true;
            }
            else
            {
                return false
            }
        }
	});
	return success;
}

function ajaxPost($link)
{
	var success =
	$.ajax({
		url: $link,
		type: "POST",
		data: $(".form").serialize()
	}).done(function(data, textstatus, xhrReq) {
		if(xhrReq.status == 278)
		{
            window.location.replace(xhrReq.getResponseHeader('Location'));
		}
        else
        {
            showMessage(data);
            if(data.success == true)
            {
                return true;
            }
            else
            {
                return false
            }
        }

	});
	return success;
}
