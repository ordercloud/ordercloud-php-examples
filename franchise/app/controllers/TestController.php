<?php
use Guzzle\Http\Client;
use Illuminate\Support\Facades\Input;
use Guzzle\Http\Exception\BadResponseException;
use Illuminate\Support\Facades\Redirect;
use Guzzle\Log\MessageFormatter;
    use Guzzle\Log\MonologLogAdapter;
    use Guzzle\Plugin\Log\LogPlugin;
    use Monolog\Handler\StreamHandler;
    use Monolog\Logger;
use Illuminate\Support\Facades\Auth;

class TestController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

    protected $layout = "layouts.tester";
    
	public function testerGet()
	{
		return View::make('tester');
	}

    public function testerPost()
    {
        $postData = Input::all();
        $client = new Client("https://test-api.ordercloud.co.za");
        $client->setDefaultOption('verify', false);
        $request = $client->get($postData["url"], array("Content-type" => "application/json", 'allow_redirects' => false));
        $request->setAuth("admin", "password");
        $response = $request->send()->json();

        return View::make('tester', array("data" => $response));
    }
}