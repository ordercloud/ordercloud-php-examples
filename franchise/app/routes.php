<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

//Requires login
Route::group(array('before' => "auth"), function() {
    Route::get('/cart', array("uses" => 'HomeController@cartGet', "as" => "cart"));
    Route::post('/cart', 'HomeController@cartPost');
    Route::get('/remove_item/{itemId}', 'HomeController@removeFromCart');
    Route::get('/checkout', "HomeController@checkout" );
    Route::get('/order/{geoId}', 'HomeController@order');
    Route::get('/myorders', 'HomeController@myorders');
    Route::get('/add_cart/{item_id}', 'HomeController@addCart');
    Route::get('/payments/{geoId?}', 'HomeController@payments');
    Route::get('/geo', "HomeController@geo" );
    Route::post('/address', "HomeController@addressPost" );
    Route::get('/address', "HomeController@addressGet" );
});
//Requries a selected store
Route::group(array('before' => "storeSelect"), function() {

    Route::get('/categories', array("as" => "categories", "uses" => "HomeController@categories"));
    Route::get('/store', 'HomeController@store');
    Route::get('/category/{category}', 'HomeController@products');
    Route::get('/product/{productId}', 'HomeController@product');

});
Route::get('/', 'HomeController@landing');
Route::get('/about', 'HomeController@about');
Route::get('/home', array("as" => "home", "uses" => 'HomeController@home'));
Route::get('/addressError', "HomeController@addressError" );
Route::get('/login', 'HomeController@loginGet');
Route::post('/login', 'HomeController@loginPost');
Route::get('/social/{action?}', "HomeController@social" );
Route::get('/register', array("as" => "register", "uses" => 'HomeController@registerGet'));
Route::post('/register/', 'HomeController@registerPost');
Route::get('/logout', 'HomeController@logout');
Route::get('laravel-version', function()
{
    $laravel = app();
    return "Your Laravel version is ".$laravel::VERSION;
});