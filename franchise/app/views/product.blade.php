@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" id="slider-wrapper" role="main">
    <div id="message-container" style="display:none; ">
        <div data-theme="b" style="" data-form="ui-body-b" class="ui-body ui-body-b ui-corner-all">
            <img style="max-width: 50px; margin-bottom: -14px;" src="/img/red_tick.png" /><h2 style="display: inline-block;">Added to cart!</h2>
        </div>
        <a data-icon="" href="/categories" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
            <span class="ui-btn-inner">
                <span class="ui-btn-text">Continue Shopping</span>
            </span>
        </a>
        <a data-icon="arrow-r" href="/cart" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
            <span class="ui-btn-inner">
                <span class=" ui-btn-text">Checkout</span>
            </span>
        </a>
    </div>
    <br>
	<div data-theme="a" data-form="ui-body-a" class="ui-body ui-body-a ui-corner-all">
		<h2 style="font-size: 1.3em;">{{$product["name"]}}</h2>
        <img style="max-width: 100%;" src="{{$product['image']}}" />
        <p style="font-size: 1.3em; font-weight: bold;" >R {{number_format((float)$product['price'], 2, '.', '')}}</p>
		<p>
            {{$product["shortDescription"]}}
		</p>
        <p>
            Options
        </p>
		<fieldset class="ui-corner-all ui-body-c ui-controlgroup ui-controlgroup-vertical" data-role="controlgroup">
	        <input type="radio" name="radio-choice-1" id="radio-choice-1" value="choice-1" checked="checked">
	        <label for="radio-choice-1">Small</label>
	        <input type="radio" name="radio-choice-1" id="radio-choice-2" value="choice-2">
	        <label for="radio-choice-2">Medium</label>
	        <input type="radio" name="radio-choice-1" id="radio-choice-3" value="choice-3">
	        <label for="radio-choice-3">Large</label>
	        <input type="radio" name="radio-choice-1" id="radio-choice-4" value="choice-4">
	        <label for="radio-choice-4">Extra Large</label>
		</fieldset>
        <p>
            Extras
        </p>
		<fieldset class="ui-corner-all ui-body-c ui-controlgroup ui-controlgroup-vertical" data-role="controlgroup">
	        <input type="checkbox" name="checkbox-v-2a" id="checkbox-v-2a">
	        <label for="checkbox-v-2a">Cheese</label>
	        <input type="checkbox" name="checkbox-v-2b" id="checkbox-v-2b">
	        <label for="checkbox-v-2b">Bacon</label>
	        <input type="checkbox" name="checkbox-v-2c" id="checkbox-v-2c">
	        <label for="checkbox-v-2c">Pineapple</label>
	    </fieldset>
        @if($product["available"] && $selectedStoreDetails["open"] === true && $selectedStoreDetails["delivering"] === true)
            <a href="/add_cart/{{$product['id']}}" data-ajax="false" class="ajaxGet ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
                <span class="ui-btn-inner">
                    <span class="ui-btn-text">Add to Cart</span>
                </span>
            </a>
        @elseif($selectedStoreDetails["delivering"] === false)
            <a href="/add_cart/{{$product['id']}}" data-ajax="false" class="ajaxGet ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
                <span class="ui-btn-inner">
                    <span class="ui-btn-text">Add to Cart (Not currently delivering)</span>
                </span>
            </a>
        @else
        <p>
            <input type="button" disabled="" value="Not available currently">
        </p>
        @endif
	</div>
</div>
@stop
