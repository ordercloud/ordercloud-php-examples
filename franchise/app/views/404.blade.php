@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	<div class="ui-bar ui-bar-c ui-corner-all" style="margin-bottom:1em;">
		<h2>Oooops, something went wrong. An email has been sent to our engineers!</h2>
	</div>
</div>
@stop