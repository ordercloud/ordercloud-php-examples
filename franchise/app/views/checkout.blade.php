@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
	<ul data-role="listview" data-inset="true">
		<li data-icon="false">
			<a onclick="event.preventDefault();">
				<h2>Order Items: </h2>
			</a>
		</li>
		@foreach ($cartItems as $id => $cartItem)
            <li data-icon="false">
            	<a href="/product/{{$cartItem->id}}" onclick="event.preventDefault();">
	                <h2>{{$cartItem->name}}</h2>
	                <p>R {{$cartItem->price}} X {{$cartItem->item_count}}</p>
	                <p><b>Item Total: </b>{{$cartItem->item_total}}</p>
            	</a>
            </li>
		@endforeach
	</ul>
	<div class="ui-bar ui-bar-b ui-corner-all" style="margin-bottom:1em;">
		<h2>Delivery Address: </h2>
		<br>
		HEre is <br>
		Some address<br>
		Detals
	</div>
	
	<a href="/order/{{$cartItems[0]->cart_id}}" class="ui-btn">Order Now</a>
</div>
@stop