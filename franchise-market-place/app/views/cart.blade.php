@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
    @if(!empty($message))
        <div data-theme="a" data-form="ui-body-a" class="ui-body ui-body-a ui-corner-all">
            @foreach($message AS $mes)
                {{$mes}}<br>
            @endforeach
        </div>
    @endif
    @if(count($cartItems) != 0 && $orderable)
    <form action="/cart" method="POST" value="" class="form" id="cart">
        <ul data-role="listview" data-inset="true">
            @foreach ($cartItems as $id => $cartItem)
            <li data-icon=delete>
                <a href="/product/{{$cartItem['item_id']}}" onclick="event.preventDefault();">
                    <h2>{{$cartItem['name']}}</h2>
                    <input data-item-count="{{$itemCount[$cartItem['item_id']]}}" data-item-price="{{$cartItem['price']}}" class="item_count_input"
                           type="range" name="product[{{$cartItem['item_id']}}]" id="slider_{{$cartItem['item_id']}}"
                           value="{{$itemCount[$cartItem['item_id']]}}" min="0" max="10" data-mini="true" data-highlight="true"
                    />
                    <br>
                    <p>R {{$cartItem['price']}}</p>
                    <p id="item_total_{{$cartItem['item_id']}}"><b>Item Total: </b>{{$itemCount[$cartItem['item_id']]}}</p>
                    <a data-theme="a" href="/remove_item/{{$cartItem['item_id']}}" class="removeFromCart" data-ajax="false">Remove from Cart</p></a>
                </a>
            </li>
            @endforeach
            <button data-link="/cart" data-theme="a" type="submit" id="cart_total" class="ui-btn ui-icon-arrow-r ui-btn-icon-right ajaxPost">Checkout</button>
        </ul>
    </form>
    @elseif(!$orderable)
    <form action="/cart" method="POST" value="" class="form" id="cart">
        <ul data-role="listview" data-inset="true">
            @foreach ($cartItems as $id => $cartItem)
            <li data-icon=delete>
                <a href="/product/{{$cartItem['item_id']}}" onclick="event.preventDefault();">
                    <h2>{{$cartItem['name']}}</h2>
                    <input data-item-count="{{$itemCount[$cartItem['item_id']]}}" data-item-price="{{$cartItem['price']}}" class="item_count_input"
                           type="range" name="product[{{$cartItem['item_id']}}]" id="slider_{{$cartItem['item_id']}}"
                           value="{{$itemCount[$cartItem['item_id']]}}" min="0" max="10" data-mini="true" data-highlight="true"
                    />
                    <br>
                    <p>R {{$cartItem['price']}}</p>
                    <p id="item_total_{{$cartItem['item_id']}}"><b>Item Total: </b>{{$itemCount[$cartItem['item_id']]}}</p>
                    <a data-theme="a" href="/remove_item/{{$cartItem['item_id']}}" class="removeFromCart" data-ajax="false">Remove from Cart</p></a>
                </a>
            </li>
            @endforeach
            <button disabled="disabled" data-link="/cart" data-theme="a" type="submit" id="cart_total" class="ui-btn ui-icon-arrow-r ui-btn-icon-right ajaxPost">Stores are closed</button>
        </ul>
    </form>
    @elseif(!$deliverable)
        <form action="/cart" method="POST" value="" class="form" id="cart">
            <ul data-role="listview" data-inset="true">
                @foreach ($cartItems as $id => $cartItem)
                <li data-icon=delete>
                    <a href="/product/{{$cartItem['item_id']}}" onclick="event.preventDefault();">
                        <h2>{{$cartItem['name']}}</h2>
                        <input data-item-count="{{$itemCount[$cartItem['item_id']]}}" data-item-price="{{$cartItem['price']}}" class="item_count_input"
                               type="range" name="product[{{$cartItem['item_id']}}]" id="slider_{{$cartItem['item_id']}}"
                               value="{{$itemCount[$cartItem['item_id']]}}" min="0" max="10" data-mini="true" data-highlight="true"
                        />
                        <br>
                        <p>R {{$cartItem['price']}}</p>
                        <p id="item_total_{{$cartItem['item_id']}}"><b>Item Total: </b>{{$itemCount[$cartItem['item_id']]}}</p>
                        <a data-theme="a" href="/remove_item/{{$cartItem['item_id']}}" class="removeFromCart" data-ajax="false">Remove from Cart</p></a>
                    </a>
                </li>
                @endforeach
                <a data-link="/cart" data-theme="a" type="submit" id="cart_total" class="ui-btn ui-icon-arrow-r ui-btn-icon-right ajaxPost">Checkout</a>
            </ul>
        </form>
    @else
    <h2>You don't have any items in your cart</h2>
    <a href="/home?branch=4" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Browse Stellenbosch Stores</span>
        </span>
    </a>
    @endif
</div>
@stop