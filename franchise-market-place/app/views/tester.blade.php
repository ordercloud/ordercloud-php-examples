@extends('layouts.tester')
@section('content')
<form id="testerForm" action="/tester" method="POST">
    <input type="text" id="url" value="/resource/users/2" name="url">
    <input type="text" id="type" name="type">
    <input type="text" id="postData" name="postData">
    <button type="submit" id="submit">Submit</button>
</form>
@if(isset($data))
    @foreach($data as $key => $value)
        <div id="field_{{$key}}">{{print_r($value, true)}}</div>
    @endforeach
@endif
@stop