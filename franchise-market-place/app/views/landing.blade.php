@extends('layouts.master')
@section('content')
<div style="text-align: center;" data-theme="c" data-role="content">
    <img src="/img/logo-1.png" />
    <br><br>
    <img src="/img/banner.png" />
    @if($store != false)
    <a href="/category/burger" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Browse {{$store['name']}}</span>
        </span>
    </a>
    <p>OR</p>
    <a href="/home" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Browse Stores</span>
        </span>
    </a>
    @else
    <a href="/home?branch=4" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Stellenbosch</span>
        </span>
    </a>
    <p>OR</p>
    <a href="/home" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-c">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Select Other</span>
        </span>
    </a>
    @endif
</div>
@stop