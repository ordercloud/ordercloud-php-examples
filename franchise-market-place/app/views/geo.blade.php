@extends('layouts.master')
@section('content')
<script type="text/javascript">
    $( document ).on("pageshow", function () {
        $.mobile.loading( 'show', {
            text: "Finding Location",
            textVisible: true
        });
    });
</script>
<script type="text/javascript" src="/js/geo.js"></script>
<div id="geo" class="geolocation_data"></div>
<div data-role="content" class="ui-content ui-body-c" role="main">
</div>
@stop