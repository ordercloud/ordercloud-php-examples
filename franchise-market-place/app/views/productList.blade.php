@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" role="main">
    <ul data-role="listview" data-inset="false" class="ui-listview ui-listview-inset ui-corner-all ui-shadow">
		@foreach($products as $id => $product)
            <li data-icon="">
                <a href="/product/{{$product['id']}}">
                    <img src='{{$product["image"]}}' class="ui-li-thumb" style="min-height: 81px; max-height: 81px; max-width:120px; min-width:120px">
                    <h2 style="padding-left: 32px;">{{$product['name']}}</h2>
                    <p style="padding-left: 32px; font-size: 15px;" >R {{number_format((float)$product['price'], 2, '.', '')}}</p>
                </a>
            </li>
        @endforeach
    </ul>
</div>
<div data-role="content" class="ui-content ui-body-c" role="main">
    <ul data-role="listview" data-inset="false" data-filter="false" data-input="#filter-for-listview">
        @foreach ($categories as $cat_id => $cat)
        <li><a href="/category/{{$cat}}">{{$cat}}</a></li>
        @endforeach
    </ul>
</div>
@stop