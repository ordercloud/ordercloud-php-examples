@extends('layouts.master')
@section('content')
<div style="text-align: center;" data-theme="c" data-role="content">
    <img src="/img/logo-1.png" />
    <br><br>
    <img src="/img/banner.png" />
    <ul data-role="listview" data-inset="true" data-filter="false" data-input="#filter-for-listview">
        @foreach ($categories as $cat_id => $cat)
        <li><a href="/category/{{$cat }}">{{ucfirst($cat)}}</a></li>
        @endforeach
    </ul>
</div>
@stop