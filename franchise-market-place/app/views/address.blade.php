@extends('layouts.master')
@section('content')
<link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
<script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
<script src='https://api.tiles.mapbox.com/mapbox.js/v2.1.1/mapbox.js'></script>
<link href='https://api.tiles.mapbox.com/mapbox.js/v2.1.1/mapbox.css' rel='stylesheet' />
<script type="text/javascript">
	$( document ).on("pageshow", function () {

		L.mapbox.accessToken = 'pk.eyJ1Ijoid2lsbGVtIiwiYSI6IjRBa2J4NEUifQ.xPT5ryIUBy1vWTs0vHOIOA';

		var map = L.map('map').setView([{{$latlong[0]}}, {{$latlong[1]}}], 17);

		L.tileLayer('https://{s}.tiles.mapbox.com/v3/examples.map-i87786ca/{z}/{x}/{y}.png', {
			maxZoom: 18,
			id: 'examples.map-i86knfo3'
		}).addTo(map);

		var mark = L.marker([{{$latlong[0]}}, {{$latlong[1]}}], {draggable: true, clickable: true}).addTo(map);

		mark.on('dragend', ondragend);
		ondragend();

		function ondragend() {
			var m = mark.getLatLng();
			$("#lat").val(m.lat);
			$("#long").val(m.lng);
		}

    });
</script>
<div data-role="content" class="ui-content ui-body-c" role="main">
    <ul data-role="listview" data-inset="true" data-filter="false" data-input="#filter-for-listview">
        <li><a href="/payments">Self-pickup</a></li>
    </ul>
    <h2>OR Pick a previous address:</h2>
    <ul data-role="listview" data-inset="true" data-filter="false" data-input="#filter-for-listview">
        @foreach ($addresses as $address)
        <li><a href="/payments/{{$address["id"]}}">{{$address["name"]}}</a></li>
        @endforeach
    </ul>
    <h2>OR select your location:</h2>
    <div  style="min-height: 550px; max-width: 800px; margin: 0 auto;" id="map"></div>
    <br><br>
	<form action="/address" method="POST" id="address">
		<label for="complex">Complex / Estate details:</label>
		<input type="text" name="complex" id="complex" value="">
		<label for="street_number">Street Number:</label>
		<input type="text" name="street_number" id="street_number" value="{{$geodata['house_number']}}">
		<label for="street_name">Street Name:</label>
		<input type="text" name="street_name" id="street_name" value="{{$geodata['street']}}">
		<label for="city">City:</label>
		<input type="text" name="city" id="city" value="{{$geodata['district_region']}}">
		<label for="post_code">Postal Code:</label>
		<input type="text" name="post_code" id="post_code" value="{{$geodata['postcode']}}">
		<button type="submit" class="ui-btn ui-body-a">Submit</button>
        @if(isset($latlong[0]))
		<input type="hidden" name="lat" id="lat" value="{{$latlong[0]}}"/>
		<input type="hidden" name="long" id="long" value="{{$latlong[1]}}"/>
        @endif
	</form>
</div>
@stop