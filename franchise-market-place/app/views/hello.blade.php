@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-c" id="slider-wrapper" role="main">
    <div data-theme="a" data-form="ui-body-a" class="ui-body ui-body-a ui-corner-all">
        @if(isset($branches))
            <p></p>
            <ul data-role="listview" data-filter="true" data-filter-placeholder="Search area" data-filter-theme="a" data-inset="true">
                @foreach($branches AS $branch)
                    <li><a href="/home?branch={{$branch['toOrganisation']['id']}}">{{$branch['toOrganisation']['name']}}</a></li>
                @endforeach
        @else
            <p>Branch: Stellenbosch</p>
            <ul data-role="listview" data-filter="true" data-filter-placeholder="Search Store" data-filter-theme="a" data-inset="true">
                @foreach($stores AS $store)
                <li>
                    <a style="min-height: 25px; padding-left: 35px;" href="/home?store={{$store['toOrganisation']['id']}}">
                    <img style="width: 20px;padding-top: 9px; padding-left: 8px;" src="/img/{{$store["status"]["colour"]}}.png">
                    {{$store['toOrganisation']['name']}} ({{$store["status"]["status"]}})</a>
                </li>
                @endforeach
            </ul>
        @endif
    </div>
</div>
@stop
