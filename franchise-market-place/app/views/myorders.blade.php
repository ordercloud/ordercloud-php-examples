@extends('layouts.master')
@section('content')
<div data-role="content" class="ui-content ui-body-a" role="main">
    <a href="/myorders" data-ajax="false" class="ui-btn ui-shadow ui-btn-corner-all ui-btn-up-a ui-btn-up-undefined">
        <span class="ui-btn-inner">
            <span class="ui-btn-text">Update Orders</span>
        </span>
    </a>
    @foreach($orders AS $order)
        <div class="ui-bar ui-bar-c ui-corner-all" style="margin-bottom:1em;">
            <h1 style="font-size: 22px;">Order <span style="color: black;" >#{{$order["id"]}}</span></h1><br><br>
            From: <span style="color: black;" >{{$order["organisation"]["name"]}}</span><br>
            Tel: <a  style="color: rgb(194, 47, 27);" data-role="none" data-theme="c" href="tel:0218081234">021 808 1234</a><br><br>
            Order Status: <br>
            @if($order["status"]["name"] == 'Ready')
                <span class="label label-warning">Ready</span><br><br>
            @elseif($order["status"]["name"] == 'Pending')
                <span class="label label-info">Pending</span><br><br>
            @elseif($order["status"]["name"] == 'Accepted')
                    <span class="label label-success">Accepted</span><br><br>
            @elseif($order["status"]["name"] == 'Collected')
                    <span class="label label-danger">Collected</span><br><br>
            @elseif($order["status"]["name"] == 'Delivered')
                    <span class="label label-primary">Delivered</span><br><br>
            @elseif($order["status"]["name"] == 'Cancelled')
                    <span class="label label-default">Cancelled/Declined</span><br><br>
            @endif

            Payment Status: <br>
            @if($order["paymentStatus"]["status"] == 'PAID')
                <span class="label label-success">Paid</span><br><br>
            @elseif($order["paymentStatus"]["status"] == 'UNPAID')
                <span class="label label-warning">Unpaid</span><br><br>
            @elseif($order["paymentStatus"]["status"] == 'PARITALLY_PAID')
                <span class="label label-info">Partially Paid</span><br><br>
            @elseif($order["paymentStatus"]["status"] == 'ACCOUNT')
                <span class="label label-danger">Account</span><br><br>
            @endif
        </div>
    @endforeach
</div>
@stop