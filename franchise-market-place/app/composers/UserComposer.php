<?php

use Ordercloud\Ordercloud\Ordercloud;

class UserComposer 
{
	public function compose($view)
	{
        if((Cookie::has("user")) && (Cookie::has("access_token")))
        {
            Log::error("Is logged in");
            $view->with(
                array("user" => unserialize(Cookie::get("user")))
            );
        }
        elseif(Cookie::has("refresh_token"))
        {
            $oc = new Ordercloud();
            try
            {
                Log::error("Attempting getNewAccessToken");
                $refreshResults = $oc->getNewAccessToken(Cookie::get("refresh_token"));

                Log::error("Attempting getUserDetails");
                $user = $oc->getUserDetails($refreshResults["access_token"]);
                Cookie::queue("access_token", $refreshResults["access_token"], 60);
                Cookie::queue("refresh_token", $refreshResults["refresh_token"], 86400);
                Cookie::queue("user", serialize($user), 60);
                Log::error("Done, setting cookie");

                $view->with(
                    array("user" => $user)
                );
            }
            catch(OrdercloudException $e)
            {
                Log::error($e);
                //Ok so this would happen if we couldn't refresh the user, so we just unset all is cookies
                $view->withCookie(Cookie::forget("user"))->withCookie(Cookie::forget("refresh_token"))->withCookie(Cookie::forget("access_token"))->with(
                    array("user" => false)
                );
            }

        }
	}
}