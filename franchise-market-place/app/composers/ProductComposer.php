<?php
use Ordercloud\Ordercloud\Ordercloud;
class ProductComposer 
{
	public function compose($view)
	{
		$storedId = Cookie::get("selectStore")["id"];
		if($storedId != null && Cache::has($storedId . " _menuItems") && !empty(Cache::get($storedId . " _menuItems")))
		{
			$arrMenu = Cache::get($storedId . " _menuItems");
			$view->with('categories', $arrMenu); return;
		}
		else if($storedId != null)
		{
			$oc = new Ordercloud();
			$menu = $oc->getMenu($storedId);
			$arrMenu = array();

			foreach($menu as $menuItem)
			{
				if($menuItem["enabled"] == true)
				{
					$arrMenu[] = $menuItem["name"];
				}
			}

			Cache::add($storedId . " _menuItems", $arrMenu, 30);
			$view->with('categories', $arrMenu); return;
		}
		else
		{
			return Redirect::to("/home");
		}
	}
}