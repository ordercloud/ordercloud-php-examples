<?php

class CartComposer 
{
	public function compose($view)
	{
        if(Cookie::has("user") != false)
        {
            $cartModel = new Cart();
            $view->with(
                'count',
                count(
                    json_decode(
                        Redis::connection()->get(
                            "user_" . unserialize(
                                Cookie::get("user")
                            )["id"]),
                        true)
                    ["cart_items"]));
        }
        else
        {
            $view->with('count', 0);
        }
	}
}