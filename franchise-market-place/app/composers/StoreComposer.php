<?php

class StoreComposer
{
	public function compose($view)
	{
        $selectedStoreDetails = false;
        if(Cookie::has("selectStore"))
        {
            $selectedStoreDetails = Cookie::get("selectStore");
            $storeStatus = array();
            if($selectedStoreDetails["open"] === true && $selectedStoreDetails["delivering"] === true)
            {
                $storeStatus["colour"] = "green";
                $storeStatus["status"] = "Online";
            }
            else if($selectedStoreDetails["open"] !== true && $selectedStoreDetails["delivering"] === true)
            {
                $storeStatus["colour"] = "blue";
                $storeStatus["status"] = "No Deliveries";
            }
            else
            {
                $storeStatus["colour"] = "red";
                $storeStatus["status"] = "Closed";
            }
            $view->with(array("selectedStoreDetails" => $selectedStoreDetails, "storeStatus" => $storeStatus));
        }
	}
}