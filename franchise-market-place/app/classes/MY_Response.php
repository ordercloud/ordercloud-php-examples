<?php
/**
 * MY_Response because
 *
 * @author Willem Van Wyk
 *
 */
class MY_response
{
	public static function redirect ($to)
    {
        header("Location: $to?login=1&redirect_url=" . urlencode(Request::header("referer")), true, 278);
        die();
    }
}